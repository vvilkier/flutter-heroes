import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_busca_herois/app/modules/home/home_module.dart';
import 'package:flutter_busca_herois/app/shared/utils/constants.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'app_widget.dart';
import 'app_controller.dart';

class AppModule extends MainModule {
  @override
  List<Bind> get binds => [
        Bind((i) => AppController()),
        Bind((i) => Dio(BaseOptions(baseUrl: URL_BASE)))
      ];

  @override
  List<Router> get routers => [Router('/', module: HomeModule())];

  @override
  Widget get bootstrap => AppWidget();
}
