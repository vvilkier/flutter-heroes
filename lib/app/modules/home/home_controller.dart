import 'package:flutter_busca_herois/app/modules/home/repositories/hero_repository.dart';
import 'package:mobx/mobx.dart';

import 'models/hero_model.dart';
part 'home_controller.g.dart';

class HomeController = _HomeControllerBase with _$HomeController;

abstract class _HomeControllerBase with Store {
  final HeroRepository repository;

  String search = '';

  @observable
  ObservableFuture<List<HeroModel>> heroes;

  _HomeControllerBase(this.repository) {}

  @action
  fetchHeroes(String name) {
    heroes = repository.searchHeroes(name).asObservable();
  }
}
