import 'package:dio/dio.dart';
import 'package:flutter_busca_herois/app/modules/home/models/hero_details_model.dart';
import 'package:flutter_busca_herois/app/modules/home/models/hero_model.dart';

class HeroRepository {
  final Dio dio;

  HeroRepository(this.dio);

  Future<List<HeroModel>> searchHeroes(String name) async {
    List<HeroModel> list = [];
    if (name.length <= 2) {
      return list;
    }
    var response = await dio.get('/search/$name');
    if (response.data['error'] == "character with given name not found") {
      throw ("Empty");
    }

    if (response.data['error'] != null) {
      throw ("API error");
    }

    list = (response.data['results'] as List)
        .map((hero) => HeroModel(hero))
        .toList();

    return list;
  }

  Future<HeroDetailsModel> getHeroById(String id) async {
    HeroDetailsModel hero;

    var response = await dio.get('/$id');

    if (response.data['error'] != null) {
      if (response.data['error'] == "character with given id not found") {
        throw ("Empty");
      }

      throw ("API error");
    }
    hero = HeroDetailsModel.fromJson(response.data);

    return hero;
  }
}
