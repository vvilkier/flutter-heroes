class HeroStatsModel {
  String intelligence;
  String strength;
  String speed;
  String durability;
  String power;
  String combat;

  HeroStatsModel(Map<String, dynamic> stats) {
    intelligence = stats["intelligence"];
    strength = stats["strength"];
    speed = stats["speed"];
    durability = stats["durability"];
    power = stats["power"];
    combat = stats["combat"];
  }
}
