import 'hero_stats_model.dart';

class HeroModel {
  String id;
  String name;
  String image;

  HeroStatsModel powerstats;

  HeroModel(Map hero) {
    id = hero["id"];
    name = hero["name"];
    image = hero["image"]["url"];
    powerstats = HeroStatsModel(hero["powerstats"]);
  }
}
