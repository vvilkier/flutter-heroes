import 'package:flutter_busca_herois/app/modules/home/home_page.dart';
import 'package:flutter_busca_herois/app/modules/home/pages/hero/hero_detail_controller.dart';
import 'package:flutter_busca_herois/app/modules/home/pages/hero/hero_detail_page.dart';
import 'package:flutter_busca_herois/app/modules/home/repositories/hero_repository.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'home_controller.dart';

class HomeModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind((i) => HomeController(i.get())),
        Bind((i) => HeroDetailController(i.get())),
        Bind((i) => HeroRepository(i.get())),
      ];

  @override
  List<Router> get routers => [
        Router('/', child: (_, args) => HomePage()),
        Router('/hero/:id',
            child: (_, args) =>
                HeroDetailPage(heroId: args.params['id'], heroData: args.data))
      ];
}
