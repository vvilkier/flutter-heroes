// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'hero_detail_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$HeroDetailController on _HeroDetailControllerBase, Store {
  final _$heroAtom = Atom(name: '_HeroDetailControllerBase.hero');

  @override
  ObservableFuture<HeroDetailsModel> get hero {
    _$heroAtom.reportRead();
    return super.hero;
  }

  @override
  set hero(ObservableFuture<HeroDetailsModel> value) {
    _$heroAtom.reportWrite(value, super.hero, () {
      super.hero = value;
    });
  }

  final _$_HeroDetailControllerBaseActionController =
      ActionController(name: '_HeroDetailControllerBase');

  @override
  dynamic getHero(String id) {
    final _$actionInfo = _$_HeroDetailControllerBaseActionController
        .startAction(name: '_HeroDetailControllerBase.getHero');
    try {
      return super.getHero(id);
    } finally {
      _$_HeroDetailControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
hero: ${hero}
    ''';
  }
}
