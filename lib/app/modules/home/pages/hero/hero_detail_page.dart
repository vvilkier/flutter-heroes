import 'package:flutter/material.dart';
import 'package:flutter_busca_herois/app/modules/home/models/hero_model.dart';
import 'package:flutter_busca_herois/app/modules/home/pages/hero/hero_detail_controller.dart';
import 'package:flutter_busca_herois/app/modules/home/widgets/hero_appearence.dart';
import 'package:flutter_busca_herois/app/modules/home/widgets/hero_biography.dart';
import 'package:flutter_busca_herois/app/modules/home/widgets/hero_connections.dart';
import 'package:flutter_busca_herois/app/modules/home/widgets/hero_status_info.dart';
import 'package:flutter_busca_herois/app/modules/home/widgets/hero_work.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:transparent_image/transparent_image.dart';

class HeroDetailPage extends StatefulWidget {
  final String heroId;
  final HeroModel heroData;

  HeroDetailPage({Key key, this.heroId, this.heroData}) : super(key: key);

  @override
  _HeroDetailPageState createState() => _HeroDetailPageState();
}

class _HeroDetailPageState extends State<HeroDetailPage>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;
  final heroController = Modular.get<HeroDetailController>();

  @override
  void initState() {
    super.initState();
    heroController.getHero(widget.heroId);
    _controller = AnimationController(
        vsync: this, duration: Duration(milliseconds: 2000));
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.black,
          title: Text(
            widget.heroData.name,
            style: TextStyle(
                color: Colors.amberAccent, letterSpacing: 2.0, fontSize: 22.0),
          ),
        ),
        body: Container(
            height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(color: Colors.black),
            child: SingleChildScrollView(child: Observer(builder: (_) {
              if (heroController.hero.value != null) {
                return Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Stack(children: <Widget>[
                      FadeInImage.memoryNetwork(
                          placeholder: kTransparentImage,
                          image: heroController.hero.value.image.url,
                          height: 500.0,
                          width: MediaQuery.of(context).size.width * 1,
                          fit: BoxFit.cover),
                      Container(),
                    ]),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Column(
                          children: <Widget>[
                            Center(
                                child:
                                    HeroStatusInfo(widget.heroData.powerstats)),
                            HeroBiography(
                              heroController.hero.value.biography.fullName != ''
                                  ? heroController.hero.value.biography.fullName
                                  : widget.heroData.name,
                              heroController.hero.value.biography.alterEgos,
                              heroController.hero.value.biography.aliases,
                              heroController.hero.value.biography.placeOfBirth,
                              heroController
                                  .hero.value.biography.firstAppearance,
                              heroController.hero.value.biography.publisher,
                              heroController.hero.value.biography.alignment,
                            ),
                            HeroAppearence(
                              heroController.hero.value.appearance.gender,
                              heroController.hero.value.appearance.race,
                              heroController.hero.value.appearance.height,
                              heroController.hero.value.appearance.weight,
                              heroController.hero.value.appearance.eyeColor,
                              heroController.hero.value.appearance.hairColor,
                            ),
                            HeroWork(heroController.hero.value.work.occupation,
                                heroController.hero.value.work.base),
                            HeroConnetions(
                                heroController
                                    .hero.value.connections.groupAffiliation,
                                heroController.hero.value.connections.relatives)
                          ],
                        )
                      ],
                    ),
                  ],
                );
              } else {
                return Container(
                    height: MediaQuery.of(context).size.height * 1,
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Center(
                              child: CircularProgressIndicator(
                            valueColor: new AlwaysStoppedAnimation<Color>(
                                Colors.amberAccent),
                          ))
                        ]));
              }
            }))));
  }
}
