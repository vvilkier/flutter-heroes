import 'package:flutter_busca_herois/app/modules/home/models/hero_details_model.dart';
import 'package:flutter_busca_herois/app/modules/home/repositories/hero_repository.dart';
import 'package:mobx/mobx.dart';
part 'hero_detail_controller.g.dart';

class HeroDetailController = _HeroDetailControllerBase
    with _$HeroDetailController;

abstract class _HeroDetailControllerBase with Store {
  final HeroRepository repository;

  @observable
  ObservableFuture<HeroDetailsModel> hero;

  _HeroDetailControllerBase(this.repository) {}

  @action
  getHero(String id) {
    hero = repository.getHeroById(id).asObservable();
  }
}
