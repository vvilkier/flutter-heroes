import 'package:flutter/material.dart';
import 'package:flutter_busca_herois/app/modules/home/models/hero_stats_model.dart';
import 'package:flutter_busca_herois/app/modules/home/widgets/single_status.dart';

class HeroStatusInfo extends StatelessWidget {
  final HeroStatsModel stats;

  HeroStatusInfo(this.stats);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width * 0.86,
      padding: EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 0.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          SingleStatus('intelligence',
              stats.intelligence != 'null' ? stats.intelligence : '-'),
          SingleStatus(
              'strength', stats.strength != 'null' ? stats.strength : '-'),
          SingleStatus('speed', stats.speed != 'null' ? stats.speed : '-'),
          SingleStatus('durability',
              stats.durability != 'null' ? stats.durability : '-'),
          SingleStatus('power', stats.power != 'null' ? stats.power : '-'),
          SingleStatus('combat', stats.combat != 'null' ? stats.combat : '-'),
        ],
      ),
    );
  }
}
