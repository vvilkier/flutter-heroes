import 'package:flutter/material.dart';
import 'package:flutter_busca_herois/app/modules/home/widgets/single_line_info.dart';

import 'multiple_line_info.dart';

class HeroWork extends StatelessWidget {
  final String occupation;
  final String base;

  HeroWork(this.occupation, this.base);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(20.0),
        child: Column(
          children: <Widget>[
            Container(
              width: (MediaQuery.of(context).size.width * 1) - 40.0,
              child: Text(
                "Trabalho",
                style: TextStyle(
                    color: Colors.amberAccent,
                    letterSpacing: 2.0,
                    fontSize: 22.0),
              ),
            ),
            SingleLineInfo(
              'Ocupacao:',
              occupation,
            ),
            SingleLineInfo(
              'Base:',
              base,
            ),
          ],
        ));
  }
}
