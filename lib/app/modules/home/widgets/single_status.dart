import 'package:flutter/material.dart';

class SingleStatus extends StatelessWidget {
  final String name;
  final String value;

  var imageStatus = {
    'intelligence': "images/icons8-cerebro-100.png",
    'strength': "images/icons8-strength-100.png",
    'speed': "images/icons8-correr-100.png",
    'durability': "images/icons8-escudo-100.png",
    'power': "images/icons8-flash-ligado-100.png",
    'combat': "images/icons8-judo-100.png"
  };

  var nameStatus = {
    'intelligence': "Inteligecia",
    'strength': "Forca",
    'speed': "Velocidade",
    'durability': "Resistencia",
    'power': "Poder",
    'combat': "Luta"
  };

  SingleStatus(this.name, this.value);

  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      Container(
          width: MediaQuery.of(context).size.width * 0.1,
          height: MediaQuery.of(context).size.width * 0.1,
          padding: EdgeInsets.all(6.0),
          margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 10.0),
          decoration: BoxDecoration(
            color: Colors.amberAccent,
            border: Border.all(
              color: Colors.amberAccent,
            ),
            borderRadius: BorderRadius.circular(50),
          ),
          child: Image.asset(imageStatus[name],
              fit: BoxFit.contain, height: 1000.0)),
      Text(
        nameStatus[name],
        style: TextStyle(
            color: Colors.amberAccent, letterSpacing: 1.0, fontSize: 10.0),
      ),
      Text(
        value,
        style:
            TextStyle(color: Colors.white, letterSpacing: 1.0, fontSize: 15.0),
      ),
    ]);
  }
}
