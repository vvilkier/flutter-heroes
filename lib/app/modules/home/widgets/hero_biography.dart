import 'package:flutter/material.dart';
import 'package:flutter_busca_herois/app/modules/home/widgets/single_line_info.dart';

import 'multiple_line_info.dart';

class HeroBiography extends StatelessWidget {
  final String fullName;
  final String alterEgos;
  final List<String> aliases;
  final String placeOfBirth;
  final String firstAppearance;
  final String publisher;
  final String alignment;

  HeroBiography(this.fullName, this.alterEgos, this.aliases, this.placeOfBirth,
      this.firstAppearance, this.publisher, this.alignment);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(20.0),
        child: Column(
          children: <Widget>[
            Container(
              width: (MediaQuery.of(context).size.width * 1) - 40.0,
              child: Text(
                "Biografia",
                style: TextStyle(
                    color: Colors.amberAccent,
                    letterSpacing: 2.0,
                    fontSize: 22.0),
              ),
            ),
            SingleLineInfo(
              'Nome Completo:',
              fullName,
            ),
            SingleLineInfo(
              'Alter egos:',
              alterEgos,
            ),
            MultipleLineInfo('Apelidos:', aliases),
            SingleLineInfo(
              'Local de nascimento:',
              placeOfBirth,
            ),
            SingleLineInfo(
              'Primeira aparicao:',
              firstAppearance,
            ),
            SingleLineInfo(
              'Editora:',
              publisher,
            ),
            SingleLineInfo(
              'Alinhamento:',
              alignment,
            ),
          ],
        ));
  }
}
