import 'package:flutter/material.dart';
import 'package:flutter_busca_herois/app/modules/home/widgets/single_line_info.dart';

import 'multiple_line_info.dart';

class HeroAppearence extends StatelessWidget {
  final String gender;
  final String race;
  final List<String> height;
  final List<String> weight;
  final String eyeColor;
  final String hairColor;

  HeroAppearence(this.gender, this.race, this.height, this.weight,
      this.eyeColor, this.hairColor);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(20.0),
        child: Column(
          children: <Widget>[
            Container(
              width: (MediaQuery.of(context).size.width * 1) - 40.0,
              child: Text(
                "Aparencia",
                style: TextStyle(
                    color: Colors.amberAccent,
                    letterSpacing: 2.0,
                    fontSize: 22.0),
              ),
            ),
            SingleLineInfo(
              'Genero:',
              gender,
            ),
            SingleLineInfo(
              'Raca:',
              race,
            ),
            MultipleLineInfo('Altura:', height),
            MultipleLineInfo('Peso:', weight),
            SingleLineInfo(
              'olhos:',
              eyeColor,
            ),
            SingleLineInfo(
              'cabelos:',
              hairColor,
            ),
          ],
        ));
  }
}
