import 'package:flutter/material.dart';

class SearchInput extends StatelessWidget {
  final Function onChangeFunction;

  SearchInput(this.onChangeFunction);

  @override
  Widget build(BuildContext context) {
    return new Container(
      decoration: BoxDecoration(
        color: Colors.black,
      ),
      child: Padding(
        padding: EdgeInsets.all(10.0),
        child: TextField(
          onChanged: (text) {
            this.onChangeFunction(text);
          },
          cursorColor: Colors.yellow,
          decoration: InputDecoration(
            labelText: 'Busque aqui',
            labelStyle: TextStyle(
              color: Colors.amber,
              letterSpacing: 2.0,
            ),
            border: OutlineInputBorder(),
            focusedBorder: OutlineInputBorder(
              borderSide: const BorderSide(color: Colors.amber, width: 2.0),
            ),
            fillColor: Colors.black,
          ),
          style: TextStyle(
            color: Colors.white,
            fontSize: 18.0,
            letterSpacing: 2.0,
          ),
          textAlign: TextAlign.center,
        ),
      ),
    );
  }
}
