import 'package:flutter/material.dart';
import 'package:flutter_busca_herois/app/modules/home/widgets/single_line_info.dart';

class HeroConnetions extends StatelessWidget {
  final String groupAffiliation;
  final String relatives;

  HeroConnetions(this.groupAffiliation, this.relatives);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(20.0),
        child: Column(
          children: <Widget>[
            Container(
              width: (MediaQuery.of(context).size.width * 1) - 40.0,
              child: Text(
                "Conexoes",
                style: TextStyle(
                    color: Colors.amberAccent,
                    letterSpacing: 2.0,
                    fontSize: 22.0),
              ),
            ),
            SingleLineInfo(
              'Equipes:',
              groupAffiliation,
            ),
            SingleLineInfo(
              'Ligacao:',
              relatives,
            ),
          ],
        ));
  }
}
