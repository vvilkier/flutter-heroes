import 'dart:math';

import 'package:flutter/material.dart';
import 'dart:ui';

class BgAnimation extends StatelessWidget {
  final AnimationController controller;

  List<String> heroBgs = [
    "images/spiderman.jpg",
    "images/captain.jpg",
    "images/batman.jpg"
  ];

  String heroImage;

  BgAnimation({this.controller})
      : bgOpacity = Tween(
          begin: 0.0,
          end: 10.0,
        ).animate(
            CurvedAnimation(parent: controller, curve: Interval(0.0, 0.150))) {
    heroImage = randomHeroImg();
  }

  final Animation<double> bgOpacity;

  void makeBlur() {
    controller.forward();
  }

  String randomHeroImg() {
    Random rnd;
    int min = 0;
    int max = heroBgs.length;
    rnd = new Random();
    print(heroBgs[min + rnd.nextInt(max - min)]);
    return heroBgs[min + rnd.nextInt(max - min)];
  }

  Widget _buildAnimation(BuildContext context, Widget child) {
    return Container(
        constraints: BoxConstraints.expand(),
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage(heroImage), fit: BoxFit.cover)),
        child: ClipRRect(
          child: BackdropFilter(
            filter: ImageFilter.blur(
                sigmaX: bgOpacity.value, sigmaY: bgOpacity.value),
            child: Container(
              alignment: Alignment.center,
              color: Colors.grey.withOpacity(0.1),
              child: Container(),
            ),
          ),
        ));
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      builder: _buildAnimation,
      animation: controller,
    );
  }
}
