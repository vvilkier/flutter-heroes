import 'package:flutter/material.dart';

class SingleLineInfo extends StatelessWidget {
  final String name;
  final String value;

  SingleLineInfo(this.name, this.value);

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.fromLTRB(0, 0.0, 0, 0),
        width: (MediaQuery.of(context).size.width * 1) - 40.0,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Column(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.fromLTRB(0, 6.0, 0, 0.0),
                  width: (MediaQuery.of(context).size.width * 0.5) - 20.0,
                  child: Text(name,
                      style: TextStyle(
                          color: Colors.grey,
                          letterSpacing: 2.0,
                          fontSize: 16.0)),
                ),
              ],
            ),
            Column(
              children: <Widget>[
                Container(
                  width: (MediaQuery.of(context).size.width * 0.5) - 20.0,
                  padding: EdgeInsets.fromLTRB(10, 6.0, 0, 0.0),
                  child: Text(value,
                      style: TextStyle(
                          color: Colors.white,
                          letterSpacing: 2.0,
                          fontSize: 16.0)),
                )
              ],
            )
          ],
        ));
  }
}
