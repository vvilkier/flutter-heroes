import 'package:flutter/material.dart';
import 'package:flutter_busca_herois/app/modules/home/models/hero_model.dart';
import 'package:flutter_busca_herois/app/modules/home/widgets/hero_status_info.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:transparent_image/transparent_image.dart';

class HeroCard extends StatelessWidget {
  final HeroModel hero;

  HeroCard(this.hero);

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 0.0),
        decoration: BoxDecoration(),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(30.0),
          child: Card(
            color: Colors.black,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0)),
            elevation: 4.0,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Stack(children: <Widget>[
                  InkWell(
                    onTap: () {
                      Modular.to.pushNamed('/hero/${hero.id}', arguments: hero);
                    },
                    child: FadeInImage.memoryNetwork(
                        placeholder: kTransparentImage,
                        image: hero.image,
                        height: 300.0,
                        width: 370,
                        fit: BoxFit.cover),
                  ),
                  Container(
                    margin: EdgeInsets.fromLTRB(15.0, 20.0, 15.0, 0.0),
                    decoration: BoxDecoration(
                        border: Border(
                          top: BorderSide(
                            //                   <--- left side
                            color: Colors.amberAccent,
                            width: 1.0,
                          ),
                          bottom: BorderSide(
                            //                    <--- top side
                            color: Colors.amberAccent,
                            width: 1.0,
                          ),
                        ),
                        gradient: LinearGradient(
                            stops: [
                              0.1,
                              0.2,
                              0.8,
                              0.9
                            ],
                            colors: [
                              Color.fromARGB(0, 0, 0, 0),
                              Color.fromARGB(120, 0, 0, 0),
                              Color.fromARGB(120, 0, 0, 0),
                              Color.fromARGB(0, 0, 0, 0),
                            ],
                            begin: Alignment.topLeft,
                            end: Alignment.bottomRight)),
                    child: Center(
                      child: Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Text(
                          hero.name,
                          style: TextStyle(
                              color: Colors.amberAccent,
                              letterSpacing: 2.0,
                              fontSize: 22.0),
                        ),
                      ),
                    ),
                  ),
                ]),
                Row(
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        HeroStatusInfo(hero.powerstats),
                        Container(
                            width: MediaQuery.of(context).size.width * 0.86,
                            height: 60,
                            alignment: Alignment.center,
                            child: InkWell(
                              onTap: () {
                                Modular.to.pushNamed('/hero/${hero.id}',
                                    arguments: hero);
                              },
                              child: Text("Ver detalhes",
                                  style: TextStyle(
                                    color: Colors.grey,
                                    fontSize: 20,
                                    letterSpacing: 2,
                                  )),
                            ))
                      ],
                    )
                  ],
                ),
              ],
            ),
          ),
        ));
  }
}
