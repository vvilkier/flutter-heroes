import 'package:flutter/material.dart';
import 'package:flutter_busca_herois/app/modules/home/widgets/hero_card.dart';
import 'package:flutter_busca_herois/app/modules/home/widgets/search_input.dart';
import 'package:flutter_busca_herois/app/modules/home/widgets/bg_animation.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'home_controller.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with TickerProviderStateMixin {
  final homeController = Modular.get<HomeController>();
  AnimationController _animationController;

  @override
  void initState() {
    super.initState();

    _animationController = AnimationController(
      vsync: this,
      duration: Duration(seconds: 2),
    );
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.black,
          title: Center(
              child: Text(
            'Flutter Heroes',
            style: TextStyle(
                color: Colors.amberAccent, letterSpacing: 2.0, fontSize: 24.0),
          )),
        ),
        body: GestureDetector(
            onTap: () {
              // call this method here to hide soft keyboard
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: Stack(children: <Widget>[
              BgAnimation(controller: _animationController.view),
              Container(
                decoration: BoxDecoration(
                    gradient: LinearGradient(colors: [
                  Color.fromARGB(150, 253, 187, 45),
                  Color.fromARGB(200, 58, 28, 113),
                ], begin: Alignment.topLeft, end: Alignment.bottomRight)),
              ),
              Container(
                  child: Column(
                children: <Widget>[
                  SearchInput((text) {
                    homeController.search = text;
                    homeController.fetchHeroes(text);
                    if (homeController.search.length > 2) {
                      _animationController.forward();
                    } else {
                      _animationController.reverse();
                    }
                  }),
                  Expanded(
                    child: Observer(
                      builder: (_) {
                        if (homeController.heroes != null) {
                          if (homeController.search.length >= 2 &&
                              homeController.heroes.value == null &&
                              homeController.heroes.error == null) {
                            return Center(
                                child: CircularProgressIndicator(
                              valueColor: new AlwaysStoppedAnimation<Color>(
                                  Colors.amberAccent),
                            ));
                          }

                          if (homeController.heroes.error != null) {
                            print(homeController.heroes.error);
                            if (homeController.heroes.error == "Empty") {
                              return Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Container(
                                      height: 200.0,
                                      padding: EdgeInsets.all(50.0),
                                      decoration: BoxDecoration(
                                        color: Colors.black,
                                      ),
                                      child: Center(
                                          child: Text(
                                              "Nenhum heroi encontrado com esse nome",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                  color: Colors.amberAccent,
                                                  letterSpacing: 2.0,
                                                  fontSize: 22.0))))
                                ],
                              );
                            } else {
                              return Container(
                                  height: 200.0,
                                  padding: EdgeInsets.all(50.0),
                                  decoration: BoxDecoration(
                                    color: Colors.black,
                                  ),
                                  child: Center(
                                      child: Text(
                                          "Booooommmmm um vilao derrubou nossa API!",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              color: Colors.amberAccent,
                                              letterSpacing: 2.0,
                                              fontSize: 22.0))));
                            }
                          }

                          if (homeController.heroes.value != null) {
                            var list = homeController.heroes.value;

                            return Padding(
                                padding: EdgeInsets.fromLTRB(20.0, 0, 20.0, 0),
                                child: ListView.builder(
                                  itemCount: list.length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    print(list[index].powerstats);
                                    return Center(child: HeroCard(list[index]));
                                  },
                                ));
                          }
                        }

                        return Container();
                      },
                    ),
                  )
                ],
              ))
            ])

            /**/
            ));
  }
}
